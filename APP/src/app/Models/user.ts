export interface IUser {

  id: string;
  rut : string;
  name: string;
  email: string;
  pass: string;

}
