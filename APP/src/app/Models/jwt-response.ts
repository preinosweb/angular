export interface IJwtResponse {

  dataUser:
  {
    username: string;
    password: string;
    accessToken?: string;
    expiresIn?: string;
  };

}
