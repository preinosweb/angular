
import { Component, OnInit } from '@angular/core';
import { UsersService } from "../services/users.service";
import { IUser } from "../Models/user";



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {



  error: any;
  columnsToDisplay = ['id','rut', 'name', 'email', 'pass'];
  datasourceUsers : Object;


  constructor(private usersService: UsersService) {

  }



  GetUsers() {

    this.usersService.getUsers()
      .subscribe((users: IUser) => {
        if (users) {
          this.datasourceUsers = users;

          // this.users = this.users.split(',');
          // console.log(this.users);



        }

      },
        error => {
          this.error = error;

        });
  }

  ngOnInit() {

    this.GetUsers();

  }


}
