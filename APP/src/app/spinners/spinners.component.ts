import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-spinners',
  templateUrl: './spinners.component.html',
  styleUrls: ['./spinners.component.css']
})
export class SpinnersComponent implements OnInit {
  showSpinner = false;
  getFakeData() {
    this.showSpinner = true;
    setTimeout(() => {
      this.showSpinner = false;
    }, 10000);
  }

  stopFakeData(){
    this.showSpinner = false;
  }

  constructor() { }

  ngOnInit() {
  }

}
