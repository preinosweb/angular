import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthService } from './services/auth.service';
import { UsersService } from "./services/users.service";

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsComponent } from './buttons/buttons.component';

import { MaterialModule } from './material.module';
import { TypographyComponent } from './typography/typography.component';
import { BadgesComponent } from './badges/badges.component';
import { SpinnersComponent } from './spinners/spinners.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AuthGuard } from "./guard/auth.guard";
import { UsersComponent } from './users/users.component';

@NgModule({
   declarations: [
      AppComponent,
      LoginComponent,
      ButtonsComponent,
      TypographyComponent,
      BadgesComponent,
      SpinnersComponent,
      NavbarComponent,
      UsersComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      MaterialModule,
      FormsModule,
      BrowserAnimationsModule
   ],
   providers: [
      AuthService,
      AuthGuard,
      UsersService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
