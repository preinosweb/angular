import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


import { AuthService } from '../../services/auth.service';
import { stringify } from 'querystring';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  protected username: string;
  protected password: string;
  protected token: string;
  protected error: any;



  constructor(private route: ActivatedRoute, private router: Router, private authenticationService: AuthService) { }

  onSubmit() {

    this.authenticationService.login(this.username, this.password)
      .subscribe(
        data => {
          if (data) {
            this.token = stringify(data);
            this.authenticationService.saveToken(this.token);
            this.router.navigate(['/']);
          }

        },
        error => {
          this.error = error;
          this.router.navigate(['login']);
        });
  }



}
