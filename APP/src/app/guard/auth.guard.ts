import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authSvc: AuthService, private router: Router) {


  }


  canActivate(): boolean {

    this.authSvc.isExpires();
    if (!this.authSvc.isExpires()) {     
      return true;
    } else {
      alert('La sesion ha expirado')
      this.router.navigate(['login']);
      return false;
    }



  }

}
