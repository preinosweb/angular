import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, OnDestroy } from '@angular/core';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnDestroy {

  title = 'Test';
  authenticate: boolean;
  fillerNav: ['home', 'login'];


  mobileQuery: MediaQueryList;




  private _mobileQueryListener: () => void;




  constructor(public authService: AuthService, private router: Router, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.authService.isExpires();

    this.mobileQuery = media.matchMedia('(max-width: 1920px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }


  isAuthenticate(): boolean {
    if (this.authService.getToken()) {
      return true;
    } else {
      return false;
    }
  }

  logouth() {

    this.authService.logout();
    this.router.navigate(['login']);
  }

}
