import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AppComponent } from './app.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TypographyComponent } from './typography/typography.component';
import { BadgesComponent } from './badges/badges.component';
import { SpinnersComponent } from './spinners/spinners.component';
import { AuthGuard  } from "./guard/auth.guard";
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path: 'home' , component : AppComponent},
  {path: 'login' , component : LoginComponent},
  {path: 'buttons' , component : ButtonsComponent, canActivate: [AuthGuard]},
  {path: 'typography' , component : TypographyComponent, canActivate: [AuthGuard]},
  {path: 'badges', component: BadgesComponent, canActivate: [AuthGuard]},
  {path: 'spinners', component: SpinnersComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsersComponent, canActivate: [AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
