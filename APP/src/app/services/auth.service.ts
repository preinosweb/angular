import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';



@Injectable()
export class AuthService {

  AUTH_SERVER = 'https://reqres.in';
  private token: string;
  private expires: string;





  constructor(private httpClient: HttpClient, private router: Router) { }


  login(username: string, password: string) {
    return this.httpClient.post(`${this.AUTH_SERVER}/api/login`, {
      email: username,
      password,
    });
  }

  logout(): void {
    this.token = '';
    localStorage.removeItem('ACCESS_TOKEN');
    localStorage.removeItem('EXPIRES_TOKEN');

  }

  saveToken(token: string): void {
    localStorage.setItem('ACCESS_TOKEN', token);
    this.token = token;
    const expires = 1200;
    const now = Date.now();
    const expiredToken = now + expires * 1000;
    localStorage.setItem('EXPIRES_TOKEN', String(expiredToken));



  }

  getToken(): string {
    if (!this.token) {
      this.token = localStorage.getItem('ACCESS_TOKEN');
    }
    return this.token;
  }



  isExpires(): boolean {
    if (this.getToken()) {
      this.expires = localStorage.getItem('EXPIRES_TOKEN');
      this.token = localStorage.getItem('ACCESS_TOKEN');
      const expires = parseInt(this.expires, 10);
      const now = Date.now();
      if (expires > 1) {
        if (now > expires) {
          localStorage.removeItem('ACCESS_TOKEN');
          localStorage.removeItem('EXPIRES_TOKEN');
          return true;
        } else {
          return false
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
  }



}







