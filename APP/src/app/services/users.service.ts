import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  name: string;

  AUTH_SERVER = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) { }

  getUsers() {
    return this.httpClient.get(`${this.AUTH_SERVER}/users`,{

    });
  }


}
